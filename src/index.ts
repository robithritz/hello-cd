import * as express from 'express';
import { createServer } from 'http';


const PORT = process.env['HELLO_PORT'] || 3000;

const app = express();

const server = createServer(app);

app.get('/', (req, res) => {
    res.send("hello world");
    res.end();
});


app.listen(PORT, () => {
    console.log(`server is listening on ${PORT}`);
})